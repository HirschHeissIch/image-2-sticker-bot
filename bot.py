#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Methods for the bot functionality."""
import logging
import traceback
import html
from configparser import ConfigParser
from io import BytesIO
from typing import IO

from telegram import (Update, InlineKeyboardMarkup, InlineKeyboardButton, Message, Bot, StickerSet,
                      ChatAction)
from telegram.error import BadRequest
from telegram.ext import CallbackContext, Dispatcher, CommandHandler, MessageHandler, Filters
from telegram.utils.helpers import mention_html
from emoji import emojize
from PIL import Image

config = ConfigParser()
config.read('bot.ini')

logger = logging.getLogger(__name__)

ADMIN: int = int(config['image-2-sticker-bot']['admins_chat_id'])
""":obj:`int`: Chat ID of the admin as read from ``bot.ini``."""
STICKER_SET_NAME: str = config['image-2-sticker-bot']['sticker_set_name']
""":obj:`str`: The name of the sticker set used to generate the sticker as read from
``bot.ini``."""
HOMEPAGE: str = 'https://hirschheissich.gitlab.io/image-2-sticker-bot/'
""":obj:`str`: Homepage of this bot."""


def info(update: Update, context: CallbackContext) -> None:
    """
    Returns some info about the bot.

    Args:
        update: The Telegram update.
        context: The callback context as provided by the dispatcher.
    """
    text = emojize(('I\'m <b>Image 2 Sticker Bot</b>. My profession is generating custom stickers '
                    ' from the pictures you send me.'
                    '\n\nTo learn more about me, please visit my homepage '
                    ':slightly_smiling_face:.'),
                   use_aliases=True)

    keyboard = InlineKeyboardMarkup.from_button(
        InlineKeyboardButton(emojize('Image 2 Sticker Bot :robot_face:', use_aliases=True),
                             url=HOMEPAGE))

    update.message.reply_text(text, reply_markup=keyboard)


def error(update: Update, context: CallbackContext) -> None:
    """
    Informs the originator of the update that an error occured and forwards the traceback to the
    admin.

    Args:
        update: The Telegram update.
        context: The callback context as provided by the dispatcher.
    """
    # Log the error before we do anything else, so we can see it even if something breaks.
    logger.error(msg="Exception while handling an update:", exc_info=context.error)

    # Inform sender of update, that something went wrong
    if update.effective_message:
        text = emojize('Something went wrong :worried:. I informed the admin :nerd_face:.',
                       use_aliases=True)
        update.effective_message.reply_text(text)

    # Get traceback
    tb_list = traceback.format_exception(None, context.error, context.error.__traceback__)
    trace = ''.join(tb_list)

    # Gather information from the update
    payload = ''
    if update.effective_user:
        payload += ' with the user {}'.format(
            mention_html(update.effective_user.id, update.effective_user.first_name))
    if update.effective_chat.username:
        payload += f' (@{html.escape(update.effective_chat.username)})'
    if update.poll:
        payload += f' with the poll id {update.poll.id}.'
    text = f'Hey.\nThe error <code>{html.escape(str(context.error))}</code> happened' \
           f'{payload}. The full traceback:\n\n<code>{html.escape(trace)}</code>'

    # Send to admin
    context.bot.send_message(ADMIN, text)


def scale_image(image: IO, outfile: IO) -> None:
    """
    Scales an image such that it fits into a 512×512 px square.

    Args:
        image: The image to scale as open file handle.
        outfile: The file to write to as open file handle.
    """
    im = Image.open(image)
    im.thumbnail((512, 512), Image.ANTIALIAS)

    if max(im.size) < 512:
        ratio = min(512 / im.size[0], 512 / im.size[1])
        im = im.resize((int(ratio * im.size[0]), int(ratio * im.size[1])))

    im.save(outfile, "PNG")
    outfile.seek(0)


def build_sticker_set_name(bot: Bot) -> str:
    """
    Builds the sticker set name given by ``STICKER_SET_NAME`` for the given bot.

    Args:
        bot: The Bot owning the sticker set.

    Returns:
        str
    """
    return '{}_by_{}'.format(STICKER_SET_NAME, bot.username)


def get_sticker_set(bot: Bot, name: str) -> StickerSet:
    """
    Get's the sticker set and creates it, if needed.

    Args:
        bot: The Bot owning the sticker set.
        name: The name of the sticker set.

    Returns:
        StickerSet
    """
    try:
        return bot.get_sticker_set(name)
    except BadRequest as e:
        if 'invalid' in str(e):
            with open('./logo/image-2-sticker-bot.png', 'rb') as sticker:
                bot.create_new_sticker_set(ADMIN, name, STICKER_SET_NAME, '🔵', png_sticker=sticker)
            return bot.get_sticker_set(name)
        else:
            raise e


def clean_sticker_set(bot: Bot) -> None:
    """
    Cleans up the sticker set, i.e. deletes all but the first sticker.

    Args:
        bot: The bot.
    """
    sticker_set = get_sticker_set(bot, build_sticker_set_name(bot))
    if len(sticker_set.stickers) > 1:
        for sticker in sticker_set.stickers[1:]:
            try:
                bot.delete_sticker_from_set(sticker.file_id)
            except BadRequest as e:
                if 'Stickerset_not_modified' in str(e):
                    pass
                else:
                    raise e


def build_and_reply(file_id: str, message: Message) -> None:
    """
    Builds the requested sticker from a image specified by the ``file_id`` and sends it back as
    reply to the ``message``. If the file is to large to handle, this will inform the user.

    Args:
        file_id: File id of the image to handle.
        message: The message to reply to.
    """
    bot: Bot = message.bot

    try:
        file = bot.get_file(file_id)
    except BadRequest as e:
        if 'too big' in str(e):
            message.reply_text('Sorry, but the file is too big. I can only handle up to 20MB.')
        else:
            raise e
        return

    sticker_set_name = build_sticker_set_name(bot)
    emojis = message.caption or '🔵'

    stream = BytesIO()
    file.download(out=stream)
    stream.seek(0)

    sticker_stream = BytesIO()
    scale_image(stream, sticker_stream)
    get_sticker_set(bot, sticker_set_name)
    try:
        bot.add_sticker_to_set(ADMIN, sticker_set_name, emojis, png_sticker=sticker_stream)
    except BadRequest as e:
        if 'Invalid sticker emojis' in str(e):
            message.reply_text('You must pass valid emojis in the caption.')
            return
        else:
            raise e

    sticker_set = get_sticker_set(bot, sticker_set_name)
    sticker_id = sticker_set.stickers[-1].file_id
    message.reply_sticker(sticker_id)


def photo_message(update: Update, context: CallbackContext) -> None:
    """
    Answers a photo message by providing the requested sticker.

    Args:
        update: The Telegram update.
        context: The callback context as provided by the dispatcher.
    """
    context.bot.send_chat_action(update.effective_user.id, ChatAction.UPLOAD_PHOTO)
    photos = update.message.photo
    photo = max(photos, key=lambda p: p.width)
    file_id = photo.file_id
    build_and_reply(file_id, update.message)
    clean_sticker_set(context.bot)


def document_message(update: Update, context: CallbackContext) -> None:
    """
    Answers a file message by providing the requested sticker.

    Args:
        update: The Telegram update.
        context: The callback context as provided by the dispatcher.
    """
    context.bot.send_chat_action(update.effective_user.id, ChatAction.UPLOAD_PHOTO)
    build_and_reply(update.message.document.file_id, update.message)
    clean_sticker_set(context.bot)


def default_message(update: Update, context: CallbackContext) -> None:
    """
    Answers any message with a note that it could not be parsed.

    Args:
        update: The Telegram update.
        context: The callback context as provided by the dispatcher.
    """
    update.message.reply_text('Sorry, but I can only handle messages containing images. '
                              'Send "/help" for more information.')


def register_dispatcher(disptacher: Dispatcher) -> None:
    """
    Adds handlers. Convenience method to avoid doing that all in the main script.

    Args:
        disptacher: The :class:`telegram.ext.Dispatcher`.
    """
    dp = disptacher

    # error handler
    dp.add_error_handler(error)

    # basic command handlers
    dp.add_handler(CommandHandler(['start', 'help'], info))

    # functionality
    dp.add_handler(MessageHandler(Filters.photo, photo_message))
    dp.add_handler(MessageHandler(Filters.document.image, document_message))
    dp.add_handler(MessageHandler(Filters.all, default_message))
