=========
Changelog
=========

Version 2.3
===========
*Released 2020-08-17*

Enhancements:

* Include the `ptbstats <https://hirschheissich.gitlab.io/ptbstats/>`_ plugin for statistics

Version 2.2
===========
*Released 2020-06-20*

Bug fixes:

* Make deletion of stickers from set more robust

Version 2.1
===========
*Released 2020-06-16*

New features:

* Can now enlarge images, too
* Sends ``ChatAction`` while computing

Minor Changes:

* Improved error handler

Version 2.0
===========
*Released 2020-06-03*

New features:

* Caption your image with an emoji to use that emoji for the generated sticker

Major changes:

* Now uses ``Bot.add_sticker_to_set`` to generate stickers. Fixes issue of Apple users not seeing the generated stickers
* ``bot.ini`` must be provided with a sticker set name (defaults to ``StickerGenerationSet``)

Version 1.0
===========
*Released 2020-05-27*

Initial release. Adds basic functionality.