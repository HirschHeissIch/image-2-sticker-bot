Image 2 Sticker Bot
===================

.. image:: https://img.shields.io/badge/python-3-blue
   :target: https://www.python.org/doc/versions/
   :alt: Supported Python versions

.. image:: https://img.shields.io/badge/backend-python--telegram--bot-blue
   :target: https://python-telegram-bot.org/
   :alt: Backend: python-telegram-bot

.. image:: https://img.shields.io/badge/documentation-is%20here-orange
   :target: https://hirschheissich.gitlab.io/image-2-sticker-bot/
   :alt: Documentation

.. image:: https://img.shields.io/badge/chat%20on-Telegram-blue
   :target: https://t.me/image_2_sticker_bot
   :alt: Telegram Chat

»Image 2 Sticker Bot« is a very simple Telegram Bot that converts images into stickers for you.
You can find it at `@image_2_sticker_bot`_.

.. _`@image_2_sticker_bot`: https://t.me/image_2_sticker_bot
