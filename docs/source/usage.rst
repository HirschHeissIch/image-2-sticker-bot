Non-functionality
-----------------

The bot commands ``/start`` and ``/help`` will display a short summary of the bots capabilities.

Generating a Sticker
--------------------

To generate a sticker from an image, simply send the image to the bot. You can also send the file as image. Note that
the bot can only handle files up to 20MB. You also don't need to worry about the image size, as the bot will
automatically resize the image to fit into a 512×512-square.
The bot will answer your message with the generated sticker.

If you like, you can add one or more emojis to the caption of your image. They will be used to describe the sticker.

Statistics
----------

This bot uses the `ptbstats <https://hirschheissich.gitlab.io/ptbstats/>`_ plugin to make some usage statistics
available. The command ``/stats`` will provide statistics on all generated stickers. Note, that those commands will only work for the admin of the bot.
